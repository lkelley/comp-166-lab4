#include <stdio.h>
#include <stdlib.h>
#include <math.h>

/*
 * Struct used for managing change denominations
 */
typedef struct {
    int value; // Value in cents
    char *singularName; // Name for single unit
    char *pluralName; // Name for multiple units
} denomination;

// Declare denominations array with correct number of denominations
denomination denominations[8];


/**
 * Reads dollar input from stdin and returns it in whole cents
 * @return Dollar amount in whole cents
 */
long readDollarInputToCents() {
    char sInput[256];
    double dInput = -1;
    fgets(sInput, sizeof(sInput), stdin); // Read string input, dollars.cents
    dInput = strtod(sInput, NULL); // Convert input to double
    dInput = round(dInput * 100); // Convert to cents and round
    return (long)dInput; // Return as a long in the form of cents
}


/**
 * Calculates the number of a given change denomination for a given change amount
 *
 * @param change Change due in cents
 * @param denominationValue Value of the change denomination in cents
 * @param singleName Singular name of the denomination (e.g. "Ten")
 * @param pluralName Plural name of the denomination (e.g. "Tens")
 * @return The remaining change due in cents
 */
long changeItem(long change, const long denominationValue, const char *singleName, const char *pluralName) {
    long billCount = change / denominationValue;

    if (billCount > 0) {
        printf("%ld %s\n", billCount, billCount > 1 ? pluralName : singleName);
    }

    change %= denominationValue;
    return change;
}


/**
 * Set up the denominations array
 */
void setup() {
    int i = 0;

    // Twenties
    denominations[i].value = 2000;
    denominations[i].singularName = "Twenty";
    denominations[i++].pluralName = "Twenties";

    // Tens
    denominations[i].value = 1000;
    denominations[i].singularName = "Ten";
    denominations[i++].pluralName = "Tens";

    // Fives
    denominations[i].value = 500;
    denominations[i].singularName = "Five";
    denominations[i++].pluralName = "Fives";

    // Toonies
    denominations[i].value = 200;
    denominations[i].singularName = "Toonie";
    denominations[i++].pluralName = "Toonies";

    // Loonies
    denominations[i].value = 100;
    denominations[i].singularName = "Loonie";
    denominations[i++].pluralName = "Loonies";

    // Quarters
    denominations[i].value = 25;
    denominations[i].singularName = "Quarter";
    denominations[i++].pluralName = "Quarters";

    // Dimes
    denominations[i].value = 10;
    denominations[i].singularName = "Dime";
    denominations[i++].pluralName = "Dimes";

    // Nickels
    denominations[i].value = 5;
    denominations[i].singularName = "Nickel";
    denominations[i].pluralName = "Nickels";
}


int main() {
    // Local vars
    long purchaseCents, changeCents, tenderedCents;

    // Set up denominations
    setup();


    /*
     * Collect purchase and tendered amounts
     */
    printf("Enter purchase amount:"); // Prompt for purchase amount
    purchaseCents = readDollarInputToCents(); // Read in purchase price and store as cents

    printf("Enter the amount tendered:");
    tenderedCents = readDollarInputToCents(); // Read tendered amount


    // Calculate change
    changeCents = tenderedCents - purchaseCents;


    /*
     * Check amounts are valid
     */
    if (tenderedCents < 0 || purchaseCents < 0) {
        printf("Amounts must both be positive\n");
        return EXIT_FAILURE;
    }

    if (tenderedCents < purchaseCents) {
        printf("Must tender at least an additional $%.2f\n", (double)abs(changeCents)/100);
        return EXIT_FAILURE;
    }

    printf("Change due is %.2f\n", changeCents / 100.0);

    /*
     * Calculate rounded change for nickels
     */
    long nickels = (long)round(changeCents / 5.0); // Determine number of nickels
    changeCents = nickels * 5; // Store rounded change
    printf("Rounded to the nearest nickel %.2f\n", nickels * 0.05); // Print rounded figure

    // Iterate over denominations and print counts
    for (int i = 0; i < sizeof(denominations) / sizeof(denomination); i++) {
        changeCents = changeItem(changeCents, denominations[i].value, denominations[i].singularName, denominations[i].pluralName);
    }

    return EXIT_SUCCESS;
}
